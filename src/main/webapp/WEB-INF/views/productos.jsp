<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html ng-app="productos">
<head>
<head>
	<script src="<%=request.getContextPath()%>/js/angular.min.js"></script>
</head>
<title>PRODUCTOS</title>
</head>
<body>
<input type="hidden" id="token" value="${token}"/>
	<div ng-controller="pController">
		<button ng-click="pFunc()">Muestra Productos</button>
		
		<div>
			<table>
				<tbody>
					<tr ng-repeat="x in lista">
					    <td>{{ x.sku }}</td>
					    <td>{{ x.nombre }}</td>
					    <td>{{ x.existencia }}</td>
					    <td>{{ x.price }}</td>
					</tr>
				</tbody>
			</table>
		</div>	
		
	</div>
	
	<div>
	<p>Generar pedido</p>
	
	</div>
	
<script>

var token = document.getElementById("token").value;

angular.module('productos', []).controller('pController', ['$scope','$http', function($scope, $http) {
		
		var contextPath =  "${pageContext.request.contextPath}";
		
		$scope.pFunc = function() {
			$http.get('http://localhost:8080/ws-rest-product-frameworks/ws/products/stock?access_token='+token).then(function(response) {
		 		$scope.lista = response.data
		 		$scope.hsku = "SKU";
		 		$scope.hnombre = "NOMBRE";
		 		$scope.hexistencias = "EXISTENCIAS";
		 		$scope.hprecio = "PRECIO";		
		 	}); 		
			   
	    };d
 		
	}]);

</script>
</body>
</html>