<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html ng-app="examen">
	<head>
		<script src="<%=request.getContextPath()%>/js/angular.min.js"></script>
	</head>
	<body>
	
	<div align="center" ng-controller="pController">
	    <form method="POST" action="<%=request.getContextPath()%>/inicio">
	    <button type="submit">ENTRAR</button>
	    <p>por tiempo, solo mando llamar al generador de token, debe ser un login</p>
	    </form>
	</div>
	
	
	<script>
		angular.module('prueba', []).controller('pController', ['$scope','$http', function($scope, $http) {
			
		    $scope.pFunc = function() {
		    	$http.get('${pageContext.request.contextPath}/consulta');
		    };
		   
		}]);
	</script>	
	</body>
</html>