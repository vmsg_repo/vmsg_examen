package com.truper.sinv.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="PEDIDOS_W")
@Getter
@Setter
public class Pedidos_w implements Serializable{

	private static final long serialVersionUID = -6525939036301862434L;

	@Id
	@Column(name="ID")
	private Integer sku;
	
	@Column(name="TOTAL")
	private String total;
	
	@Column(name="DATE_SALE")
	private Date date_sale = new Date();
	
	@Column(name="USERNAME")
	private BigDecimal username;
}
