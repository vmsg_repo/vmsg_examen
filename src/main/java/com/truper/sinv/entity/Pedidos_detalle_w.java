package com.truper.sinv.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name="PEDIDOS_DETALLE_W")
@Getter
@Setter
public class Pedidos_detalle_w implements Serializable{

	private static final long serialVersionUID = 1353147195006115108L;

	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="ID_PEDIDO")
	private Integer id_pedido;
	
	@Column(name="SKU")
	private String sku;
	
	@Column(name="AMOUNT")
	private BigDecimal amount;
	
	@Column(name="PRICE")
	private BigDecimal price;
}
