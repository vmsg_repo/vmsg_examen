package com.truper.samples.service;


public interface LoginService {
	
	String getToken(String username, String password);
			
}
