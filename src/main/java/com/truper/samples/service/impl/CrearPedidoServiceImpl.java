package com.truper.samples.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.truper.samples.dao.CrearPedidoDao;
import com.truper.samples.service.CrearPedidoService;
import com.truper.sinv.entity.Pedidos_detalle_w;
import com.truper.sinv.entity.Pedidos_w;
@Service
public class CrearPedidoServiceImpl implements CrearPedidoService {
	
	@Autowired
	private CrearPedidoDao crearPedidoDao;
	
	public Boolean generaPedido(Pedidos_w pedido, Pedidos_detalle_w detalle){
		
		int result = crearPedidoDao.creaPedido(pedido,detalle);
		if(result == 1){
			return true;
		}
		return false;
	}

}