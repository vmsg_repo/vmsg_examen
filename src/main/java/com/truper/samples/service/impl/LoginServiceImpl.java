package com.truper.samples.service.impl;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.truper.samples.dao.UserDAO;
import com.truper.samples.service.LoginService;
import com.truper.sinv.vo.UserVO;

/**
 * 
 * @author cgarcias
 * @since 07/06/2017
 */
@Service(value="administradorLoginService")
public class LoginServiceImpl implements UserDetailsService, LoginService {
	
	String JSON_TOKKEN="";
	
	@Autowired
	private UserDAO userDAO;
	
	/**
	 * Carga el UserDetails con el user name y la contraseņa es comparada internamente con Spring-Security
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserVO user = userDAO.getByUser(username);
		return makeGrantedAuthorities(user, false);
	}
	
	/**
	 * Genera el usuario, con los permisos y password para ser comparado por spring security
	 * @param uUser
	 * @param boolToken
	 * @return UserVO
	 */
	private UserVO makeGrantedAuthorities(UserVO uUser, boolean boolToken) {
		Set<GrantedAuthority> sGrantedAuthorities = new HashSet<>();
        sGrantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + uUser.getRol()));
		uUser.setAuthorities(sGrantedAuthorities);
		uUser.setPassword(uUser.getPassword());
		
		return uUser;
	}

	@Override
	public String getToken(String username, String password) {
		String JSON_TOKKEN="";
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			String url  = "http://localhost:8080/ws-rest-product-frameworks/oauth/token?grant_type=password&client_id=restapp&client_secret=restapp&username=app&password=pruebas";
			ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
			if (response.getStatusCode()==HttpStatus.OK){
				ObjectMapper mapper = new ObjectMapper();
				JsonNode root;
				try {
					root = mapper.readTree(response.getBody());
					JsonNode jsonToken = root.path("access_token");
					JSON_TOKKEN = jsonToken.getTextValue();
				} catch (IOException e) {
					return "error";
				}
			}
		} catch (HttpClientErrorException k1) {            
		    return "error";
		} catch (RestClientException k) {
		    return "error";
		}
		return JSON_TOKKEN;
	}

}