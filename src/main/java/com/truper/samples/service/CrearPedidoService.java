package com.truper.samples.service;

import com.truper.sinv.entity.Pedidos_detalle_w;
import com.truper.sinv.entity.Pedidos_w;

public interface CrearPedidoService {
	
	Boolean generaPedido(Pedidos_w pedido, Pedidos_detalle_w detalle);

}
