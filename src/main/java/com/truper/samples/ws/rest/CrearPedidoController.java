package com.truper.samples.ws.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.truper.samples.models.DatosPedido;
import com.truper.samples.service.CrearPedidoService;

@Controller
@RequestMapping("/ws/pedido")
public class CrearPedidoController {
	
	@Autowired
	private CrearPedidoService crearPedidoService;
	
	@RequestMapping(value = "/crea", method = RequestMethod.POST)
	@ResponseBody
	public Boolean obtenerProduct(@RequestBody DatosPedido datosPedido){
		
		return crearPedidoService.generaPedido(datosPedido.getPedido(), datosPedido.getDetalle());
	}

}
