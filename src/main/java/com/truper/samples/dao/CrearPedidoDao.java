package com.truper.samples.dao;

import com.truper.sinv.entity.Pedidos_detalle_w;
import com.truper.sinv.entity.Pedidos_w;

public interface CrearPedidoDao {

	int creaPedido(Pedidos_w pedido, Pedidos_detalle_w detalle);

}