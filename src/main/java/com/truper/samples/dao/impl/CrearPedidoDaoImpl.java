package com.truper.samples.dao.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.truper.samples.dao.CrearPedidoDao;
import com.truper.sinv.entity.Pedidos_detalle_w;
import com.truper.sinv.entity.Pedidos_w;

@Repository
public class CrearPedidoDaoImpl implements CrearPedidoDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public int creaPedido(Pedidos_w pedido, Pedidos_detalle_w detalle){
		sessionFactory.getCurrentSession().save(pedido);
		sessionFactory.getCurrentSession().save(detalle);
		return 1;
	}

}
