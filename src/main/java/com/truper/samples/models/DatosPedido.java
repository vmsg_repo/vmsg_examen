package com.truper.samples.models;

import lombok.Data;

import com.truper.sinv.entity.Pedidos_detalle_w;
import com.truper.sinv.entity.Pedidos_w;

@Data
public class DatosPedido {

	private Pedidos_detalle_w detalle;
	private Pedidos_w pedido;
}
