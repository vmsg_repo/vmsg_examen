package com.truper.samples.view;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.truper.samples.service.impl.LoginServiceImpl;

@Controller
public class CargaVista {
	
	@Autowired
	private LoginServiceImpl administradorLoginService;

	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String home(HttpSession session) {
		
        return "index";
    }
	
	@RequestMapping(value = {"/inicio"}, method = RequestMethod.POST)
	@ResponseBody
    public ModelAndView home(@RequestBody String username) {
		ModelAndView mav = null;
		String token = administradorLoginService.getToken("app", "pruebas");
		if(token.equals("error")){
			mav = new ModelAndView("error");
		    
		}else{
			mav = new ModelAndView("productos");
			mav.addObject("token", token);
		}
		return mav;
    }
	
}